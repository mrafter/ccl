using System;
using System.Collections.Generic;
using Xunit;

namespace SupermarketKata.Tests
{
    public class CheckoutServiceTest
    {
        private CheckoutService service;

        public CheckoutServiceTest()
        {
            var itemDatabase = new ItemDatabase(TestData.ItemDictionary);

            service = new CheckoutService(itemDatabase);
        }

        [Fact]
        public void GetTotalPrice_ScanNoItems_Returns0()
        {
            // Arrange

            // Act
            var result = service.GetTotalPrice();

            // Assert
            Assert.Equal(0, result);
        }

        [Fact]
        public void GetTotalPrice_ScanOneItem_ReturnsItemPrice()
        {
            // Arrange
            service.Scan(TestData.ItemA.Sku);

            // Act
            var result = service.GetTotalPrice();

            // Assert
            Assert.Equal(TestData.ItemA.Price, result);
        }

        [Fact]
        public void GetTotalPrice_ScanMultipleOfSameItem_ReturnsSummedPrice()
        {
            // Arrange
            int expectedPrice = 100; // 50*2

            service.Scan(TestData.ItemA.Sku);
            service.Scan(TestData.ItemA.Sku);

            // Act
            var result = service.GetTotalPrice();

            // Assert
            Assert.Equal(expectedPrice, result);
        }

        [Fact]
        public void GetTotalPrice_ScanMultipleItems_ReturnsSummedPrice()
        {
            // Arrange
            int expectedPrice = 115; // 50+30+20+15

            service.Scan(TestData.ItemA.Sku);
            service.Scan(TestData.ItemB.Sku);
            service.Scan(TestData.ItemC.Sku);
            service.Scan(TestData.ItemD.Sku);

            // Act
            var result = service.GetTotalPrice();

            // Assert
            Assert.Equal(expectedPrice, result);
        }

        [Fact]
        public void GetTotalPrice_ScanMultipleItemsWithOffers_ReturnsReducedPrice()
        {
            // Arrange
            var basketItems = new List<BasketItem>
            {
                TestData.ItemA, TestData.ItemA, TestData.ItemA, // 130
                TestData.ItemB, TestData.ItemB, // 45
                TestData.ItemC // 20
            };

            basketItems.ForEach(item => service.Scan(item.Sku));

            const int expectedPrice = 195; // 130+45+20

            // Act
            var result = service.GetTotalPrice();

            // Assert
            Assert.Equal(expectedPrice, result);
        }

        [Fact]
        public void GetTotalPrice_ScanMultipleItemsWithOffersAndIncompleteOffers_OnlyReducesCompleteOffers()
        {
            // Arrange
            var basketItems = new List<BasketItem>
            {
                TestData.ItemA, TestData.ItemA, TestData.ItemA, TestData.ItemA, // 180
                TestData.ItemB, TestData.ItemB, TestData.ItemB, // 75
                TestData.ItemC, // 20
                TestData.ItemD, TestData.ItemD, TestData.ItemD // 45
            };

            basketItems.ForEach(item => service.Scan(item.Sku));

            const int expectedPrice = 320; // 180+75+20+45

            // Act
            var result = service.GetTotalPrice();

            // Assert
            Assert.Equal(expectedPrice, result);
        }

        [InlineData(null)]
        [InlineData("")]
        [Theory]
        public void Scan_NullItemSku_ThrowException(string itemSku)
        {
            // Arrange
            string expectedException = "Value cannot be null";

            // Act
            Exception ex = Assert.Throws<ArgumentNullException>(() => service.Scan(itemSku));

            // Assert
            Assert.Contains(expectedException, ex.Message);
        }

        [Fact]
        public void Scan_ItemNotFound_ReturnsFalse()
        {
            // Act
            var result = service.Scan("Z");

            // Assert
            Assert.False(result);
        }

        [Fact]
        public void Scan_ItemFound_ReturnsTrue()
        {
            // Act
            var result = service.Scan(TestData.ItemA.Sku);

            // Assert
            Assert.True(result);
        }

        private class TestData
        {
            internal static Dictionary<string, BasketItem> ItemDictionary = new Dictionary<string, BasketItem>()
            {
                { ItemA.Sku, ItemA },
                { ItemB.Sku, ItemB },
                { ItemC.Sku, ItemC },
                { ItemD.Sku, ItemD }
            };

            internal static BasketItem ItemA => new BasketItem("A", 50, new QuantityForPriceOffer(3, 130));
            internal static BasketItem ItemB => new BasketItem("B", 30, new QuantityForPriceOffer(2, 45));
            internal static BasketItem ItemC => new BasketItem("C", 20);
            internal static BasketItem ItemD => new BasketItem("D", 15);
        }
    }
    
}
