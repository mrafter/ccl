﻿using Xunit;

namespace SupermarketKata.Tests
{
    public class QuantityForPriceOfferTest
    {
        private const int itemPrice = 50;
        private BasketItem item = new BasketItem("A", itemPrice, new QuantityForPriceOffer(3, 130));

        [Fact]
        void CalculatePrice_OfferIncomplete_ReturnsSummedUnitPrice()
        {
            // Arrange
            const int itemQuantity = 2;
            const int expectedPrice = 100; // unit price * 2

            // Act
            var result = item.Offer.CalculatePrice(itemQuantity, itemPrice);

            // Assert
            Assert.Equal(expectedPrice, result);
        }

        [InlineData(3, 130)] // offer price
        [InlineData(6, 260)] // (offer price * 2)
        [Theory]
        void CalculatePrice_OffersComplete_ReturnsReducedPrice(int itemQuantity, int expectedPrice)
        {
            // Arrange

            // Act
            var result = item.Offer.CalculatePrice(itemQuantity, itemPrice);

            // Assert
            Assert.Equal(expectedPrice, result);
        }

        [InlineData(4, 180)] // offer price + (unit price)
        [InlineData(5, 230)] // offer price + (unit price * 2)
        [InlineData(7, 310)] // (offer price * 2) + unit price
        [InlineData(8, 360)] // (offer price * 2) + (unit price * 2)
        [Theory]
        void CalculatePrice_OfferCompletePlusIncompleteOffers_OnlyReducesCompleteOffers(int itemQuantity, int expectedPrice)
        {
            // Arrange

            // Act
            var result = item.Offer.CalculatePrice(itemQuantity, itemPrice);

            // Assert
            Assert.Equal(expectedPrice, result);
        }
    }
}
