﻿using System;
using System.Collections.Generic;

namespace SupermarketKata
{
    public interface IItemDatabase
    {
        BasketItem GetItemFromCode(string itemSku);
    }

    public class ItemDatabase : IItemDatabase
    {
        private readonly Dictionary<string, BasketItem> items;

        public ItemDatabase(Dictionary<string, BasketItem> items)
        {
            this.items = items ?? throw new ArgumentNullException(nameof(items));
        }

        public BasketItem GetItemFromCode(string itemSku)
        {
            return items.TryGetValue(itemSku, out BasketItem result) ? result : null;
        }
    }
}
