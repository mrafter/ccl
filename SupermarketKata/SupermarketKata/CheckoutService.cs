﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SupermarketKata
{
    public interface ICheckoutService
    {
        bool Scan(string itemSku);
        int GetTotalPrice();
    }

    public class CheckoutService : ICheckoutService
    {
        private IItemDatabase itemDatabase;
        private List<BasketItem> basketItems = new List<BasketItem>();

        public CheckoutService(IItemDatabase itemDatabase)
        {
            this.itemDatabase = itemDatabase ?? throw new ArgumentNullException(nameof(itemDatabase));
        }

        public int GetTotalPrice()
        {
            int sum = 0;

            foreach (BasketItem item in basketItems.Distinct())
            {
                int itemCount = basketItems.Count(bi => bi.Sku == item.Sku);

                sum += item.Offer?.CalculatePrice(itemCount, item.Price) ?? item.Price * itemCount;
            }

            return sum;
        }

        public bool Scan(string itemSku)
        {
            if (string.IsNullOrEmpty(itemSku))
            {
                throw new ArgumentNullException(nameof(itemSku));
            }

            BasketItem scannedItem = itemDatabase.GetItemFromCode(itemSku);

            if (scannedItem != null)
            {
                basketItems.Add(scannedItem);
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
