﻿using System;
using System.Collections.Generic;

namespace SupermarketKata
{
    class App
    {
        public void Run()
        {
            var itemDictionary = new Dictionary<string, BasketItem>
            {
                { "A", new BasketItem("A", 50, new QuantityForPriceOffer(3, 130)) },
                { "B", new BasketItem("B", 30, new QuantityForPriceOffer(2, 45)) },
                { "C", new BasketItem("C", 20) },
                { "D", new BasketItem("D", 15) }
            };

            var checkoutService = new CheckoutService(new ItemDatabase(itemDictionary));

            Console.WriteLine("List of items available to scan:");
            Console.WriteLine("================================");

            foreach (var item in itemDictionary)
            {
                string offerMessage = item.Value.Offer?.GetMessage() ?? "";

                Console.WriteLine($"{item.Value.Sku}: {item.Value.Price} {offerMessage}");
            }

            Console.WriteLine("================================");
            Console.WriteLine("Enter the itemcode to scan an item into basket, or 'end' to quit..");

            while (true)
            {
                var consoleInput = Console.ReadLine();

                if (consoleInput.ToLower() == "end" || consoleInput.ToLower() == "'end'")
                {
                    break;
                }

                if (checkoutService.Scan(consoleInput.ToUpper()))
                {
                    var total = checkoutService.GetTotalPrice();
                    Console.WriteLine($"Running basket total: {total}");
                }
                else
                {
                    Console.WriteLine($"Unrecognised itemcode {consoleInput}");
                }
            }
        }
    }
}
