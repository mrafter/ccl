﻿namespace SupermarketKata
{
    public interface IOffer
    {
        int CalculatePrice(int itemQuantity, int itemPrice);
        string GetMessage();
    }

    public class QuantityForPriceOffer : IOffer
    {
        private readonly int reductionQuantity;
        private readonly int reductionPrice;

        public QuantityForPriceOffer(int reductionQuantity, int reductionPrice)
        {
            this.reductionQuantity = reductionQuantity;
            this.reductionPrice = reductionPrice;
        }

        public int CalculatePrice(int itemQuantity, int itemPrice)
        {
            int setsToReduce = itemQuantity / reductionQuantity;

            if (setsToReduce < 1)
            {
                return itemQuantity * itemPrice;
            }

            int remainder = itemQuantity % reductionQuantity;

            int offerPrice = (setsToReduce * reductionPrice) + (itemPrice * remainder);

            return offerPrice;
        }

        public string GetMessage()
        {
            return $"({reductionQuantity} for {reductionPrice})";
        }
    }
}
