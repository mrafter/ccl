﻿namespace SupermarketKata
{
    public class BasketItem
    {
        public string Sku { get; }
        public int Price { get; }
        public IOffer Offer { get; }

        public BasketItem(string sku, int price, IOffer offer = null)
        {
            Sku = sku;
            Price = price;
            Offer = offer;
        }
    }
}
